Populated from:
https://github.com/dreifacherspass/cyberpunk-technotronic-icon-theme and simplified dir structure and contents for Elive E16 releases.
On 20231304-commit d7d97e4.


Original README.md text:

# cyberpunk-technotronic-icon-theme



a blue-purple (oomox) gradient full-icon theme.

For all of us that love cyberpunk, neon, outrun, synthwave and retrofuture aesthetics.

**Feel free to ask for new icons**

The Ultimate-Punk-Suru++ icon theme was awesome. But it hasn't received updates for years and it also uses the same generic icons for a multitude of applications.

I am creating an icon pack based on the Ultimate-Punk-Suru++, removing the generic icons and adding new ones specific for each application, mimetype, action etc. I am colouring all icons using our beloved blue-purple gradient, so that the icon pack has a uniform look.

* CC BY-SA 2.0 © [Ultimate-Punk-Suru++](https://www.opendesktop.org/p/1333537/) by [rtl88](https://www.opendesktop.org/u/rtl88)
