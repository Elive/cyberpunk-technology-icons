### Notes/issues to using Cyberpunk/BeautyLine in E16.

1.  E16 menu will have to be set to not show icons...it doesn't support svg files.

2.  Unison brings in it's own icon including version number in the .desktop, currently:

   'unison-2.51+4.11.1-gtk.desktop'. This will break if version the no. changes.
